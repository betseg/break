use tetra::{
    graphics::{self, Font, Text, Texture},
    input::{self, Key},
    math::Vec2,
    window, Context, State,
};

use crate::consts::*;
use crate::entity::Entity;

pub struct GameState {
    bricks: Vec<Entity>,
    ball: Entity,
    paddle: Entity,
    is_paused: bool,
    score: (Text, u32),
}

impl GameState {
    pub fn new(ctx: &mut Context) -> tetra::Result<GameState> {
        let ball_tex = Self::create_texture(ctx, BALL_WIDTH, BALL_HEIGHT, BALL_COLOR)?;
        let paddle_tex = Self::create_texture(ctx, PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_COLOR)?;
        let brick_texs = BRICK_COLORS
            .iter()
            .map(|c| Self::create_texture(ctx, BRICK_WIDTH, BRICK_HEIGHT, *c))
            .collect::<tetra::Result<Vec<_>>>()?;

        let paddle_pos = Vec2::new(
            (WINDOW_WIDTH - paddle_tex.height() as f32) / 2.,
            WINDOW_HEIGHT - PADDLE_HEIGHT as f32 - MARGIN,
        );

        let ball = Entity::with_velocity(ball_tex, paddle_pos + STARTING_POS, STARTING_VEL);
        let paddle = Entity::new(paddle_tex, paddle_pos);

        let mut bricks = Vec::new();
        for i in 0..BRICK_ROWS {
            for j in 0..BRICK_COLS {
                bricks.push(Entity::new(
                    brick_texs[i].clone(),
                    Vec2::new(
                        HORZ_OUT_MGN + j as f32 * HORZ_IN_MGN,
                        VERT_OUT_MGN + i as f32 * VERT_IN_MGN,
                    ),
                ));
            }
        }

        Ok(GameState {
            bricks,
            ball,
            paddle,
            is_paused: false,
            score: (
                Text::new(format!("{}{}", SCORE_TEXT, 0), Font::default(), TEXT_SIZE),
                0,
            ),
        })
    }

    fn create_texture(
        ctx: &mut Context,
        width: i32,
        height: i32,
        color: [u8; 4],
    ) -> tetra::Result<Texture> {
        Texture::from_rgba(
            ctx,
            width,
            height,
            &std::iter::repeat(&color)
                .take((width * height) as usize)
                .flatten()
                .copied()
                .collect::<Vec<_>>(),
        )
    }
}

impl State for GameState {
    fn update(&mut self, ctx: &mut Context) -> tetra::Result {
        if self.is_paused {
            if input::is_key_pressed(ctx, Key::Space) {
                self.score
                    .0
                    .set_content(format!("{}{}", SCORE_TEXT, self.score.1 * SCORE_MULT));
                self.is_paused = false;
            }
            return Ok(());
        }
        if input::is_key_pressed(ctx, Key::Space) {
            self.is_paused = true;
            self.score.0.set_content(PAUSED_TEXT);
            return Ok(());
        }

        self.ball.position += self.ball.velocity;

        if (input::is_key_down(ctx, Key::A) || input::is_key_down(ctx, Key::Left))
            && self.paddle.position.x > MARGIN
        {
            self.paddle.position.x -= PADDLE_SPEED;
        }
        if (input::is_key_down(ctx, Key::D) || input::is_key_down(ctx, Key::Right))
            && self.paddle.position.x + (PADDLE_WIDTH as f32) < WINDOW_WIDTH - MARGIN
        {
            self.paddle.position.x += PADDLE_SPEED;
        }

        let ball_bounds = self.ball.bounds();
        let paddle_bounds = self.paddle.bounds();

        for i in 0..self.bricks.len() {
            if self.bricks[i].bounds().intersects(&ball_bounds) {
                let ball_prev_pos = self.ball.center() - self.ball.velocity;
                let brick = self.bricks.remove(i).bounds();

                if ball_prev_pos.x < brick.left() || ball_prev_pos.x > brick.right() {
                    self.ball.velocity.x = -self.ball.velocity.x;
                } else if ball_prev_pos.y < brick.bottom() || ball_prev_pos.y > brick.top() {
                    self.ball.velocity.y = -self.ball.velocity.y;
                }
                self.ball.position += self.ball.velocity;

                self.score.1 += self.ball.velocity.magnitude() as u32;
                self.score
                    .0
                    .set_content(format!("{}{}", SCORE_TEXT, self.score.1 * SCORE_MULT));
                break;
            }
        }

        if self.bricks.is_empty() {
            window::quit(ctx);
            println!("noice");
            println!("score: {}", self.score.1 * SCORE_MULT);
        }

        if ball_bounds.intersects(&paddle_bounds) {
            self.ball.position -= self.ball.velocity;
            let offset = (self.paddle.center().x - self.ball.center().x) / self.paddle.width();
            self.ball.velocity =
                Vec2::new(0., self.ball.velocity.magnitude()).rotated_z(BOUNCINESS * offset);
            self.ball.velocity.y = -self.ball.velocity.y;
            if self.ball.velocity.magnitude() < 10. {
                self.ball.velocity *= BALL_ACCELERATION;
            }
        } else if self.ball.position.y <= MARGIN {
            self.ball.velocity.y = -self.ball.velocity.y;
        } else if self.ball.position.y >= WINDOW_HEIGHT {
            window::quit(ctx);
            println!("nt");
            println!("score: {}", self.score.1 * SCORE_MULT);
        }

        if self.ball.position.x <= MARGIN
            || self.ball.position.x + self.ball.texture.width() as f32 >= WINDOW_WIDTH - MARGIN
        {
            self.ball.velocity.x = -self.ball.velocity.x;
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> tetra::Result {
        graphics::clear(ctx, BACKGROUND);

        graphics::draw(ctx, &self.score.0, SCORE_POS);
        graphics::draw(ctx, &self.ball.texture, self.ball.position);
        graphics::draw(ctx, &self.paddle.texture, self.paddle.position);

        for i in self.bricks.iter() {
            graphics::draw(ctx, &i.texture, i.position);
        }

        Ok(())
    }
}
