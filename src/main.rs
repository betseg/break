use tetra::ContextBuilder;

mod consts;
mod entity;
mod gamestate;

fn main() -> tetra::Result {
    ContextBuilder::new("brek blok", 640, 480)
        .quit_on_escape(true)
        .build()?
        .run(gamestate::GameState::new)
}
