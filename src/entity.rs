use tetra::{
    graphics::{Rectangle, Texture},
    math::Vec2,
};

pub struct Entity {
    pub texture: Texture,
    pub position: Vec2<f32>,
    pub velocity: Vec2<f32>,
}

impl Entity {
    pub const fn new(texture: Texture, position: Vec2<f32>) -> Self {
        Self::with_velocity(texture, position, Vec2 { x: 0., y: 0. })
    }

    pub const fn with_velocity(texture: Texture, position: Vec2<f32>, velocity: Vec2<f32>) -> Self {
        Self {
            texture,
            position,
            velocity,
        }
    }

    pub fn bounds(&self) -> Rectangle {
        Rectangle::new(
            self.position.x,
            self.position.y,
            self.width(),
            self.height(),
        )
    }

    pub fn center(&self) -> Vec2<f32> {
        Vec2::new(
            self.position.x + self.width() / 2.,
            self.position.y + self.height() / 2.,
        )
    }

    pub fn width(&self) -> f32 {
        self.texture.width() as f32
    }

    pub fn height(&self) -> f32 {
        self.texture.height() as f32
    }
}
