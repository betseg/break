use tetra::{graphics::Color, math::Vec2};

pub const WINDOW_WIDTH: f32 = 640.0;
pub const WINDOW_HEIGHT: f32 = 480.0;
pub const MARGIN: f32 = PADDLE_HEIGHT as f32 * 1.5;
// darkslategrey
pub const BACKGROUND: Color = Color::rgb(0.18, 0.31, 0.31);

pub const BALL_WIDTH: i32 = 10;
pub const BALL_HEIGHT: i32 = 10;
pub const BALL_COLOR: [u8; 4] = [255; 4];
pub const BALL_ACCELERATION: f32 = 1.04;
pub const STARTING_VEL: Vec2<f32> = Vec2::new(3., 3.);
pub const STARTING_POS: Vec2<f32> = Vec2::new(-50., -75.);

pub const PADDLE_WIDTH: i32 = 100;
pub const PADDLE_HEIGHT: i32 = 10;
// dodgerblue
pub const PADDLE_COLOR: [u8; 4] = [30, 144, 255, 255];
pub const BOUNCINESS: f32 = 1.;
pub const PADDLE_SPEED: f32 = 8.;

pub const BRICK_WIDTH: i32 = 45;
pub const BRICK_HEIGHT: i32 = 35;
pub const BRICK_ROWS: usize = 4;
pub const BRICK_COLS: usize = 12;
pub const HORZ_OUT_MGN: f32 = BRICK_WIDTH as f32 / 2.;
pub const VERT_OUT_MGN: f32 = BRICK_HEIGHT as f32 * 1.5;
pub const HORZ_IN_MGN: f32 =
    (WINDOW_WIDTH - 2. * HORZ_OUT_MGN - BRICK_WIDTH as f32) / (BRICK_COLS as f32 - 1.);
pub const VERT_IN_MGN: f32 = HORZ_IN_MGN - (BRICK_WIDTH - BRICK_HEIGHT) as f32;

// lightsalmon, khaki, lightgreen, lightskyblue
pub const BRICK_COLORS: [[u8; 4]; 4] = [
    [255, 160, 122, 255],
    [240, 230, 140, 255],
    [144, 238, 144, 255],
    [135, 206, 250, 255],
];

pub const SCORE_MULT: u32 = 25;
pub const SCORE_POS: Vec2<f32> = Vec2::new(10., 10.);
pub const TEXT_SIZE: f32 = 20.;
pub const SCORE_TEXT: &'static str = "Score: ";
pub const PAUSED_TEXT: &'static str = "Paused.";
